package com.parisa00100.dehkadehhooshmand;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.parisa00100.dehkadehhooshmand.NewClasses.Shop;
import com.parisa00100.dehkadehhooshmand.NewClasses.Social;

import java.util.List;

public class SocialAdapter extends RecyclerView.Adapter<SocialAdapter.MyViewHoulder> {
    Context context;
    List<Social> list;

    public SocialAdapter(Context context, List<Social> list) {
        this.context=context;
        this.list = list;
    }


    @NonNull
    @Override
    public SocialAdapter.MyViewHoulder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.social, viewGroup, false);

        return new SocialAdapter.MyViewHoulder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull SocialAdapter.MyViewHoulder houlder, int position) {
        Social social = list.get(position);
        Glide.with(context).load("http://idpz.ir/assets/images/137/" + social.getPic()).into(houlder.imageView);
        houlder.title1.setText("پاکدشت");
        houlder.title2.setText(social.getSotext());
    }


    @Override
    public int getItemCount() {
        try {
            return list.size();
        } catch (Exception e) {
        }
        return 0;
    }


    public class MyViewHoulder extends RecyclerView.ViewHolder {
        RoundedImageView imageView;
        TextView title1;
        TextView title2;

        public MyViewHoulder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.img);
            title1 = itemView.findViewById(R.id.txt1);
            title2 = itemView.findViewById(R.id.txt2);


        }
    }


}