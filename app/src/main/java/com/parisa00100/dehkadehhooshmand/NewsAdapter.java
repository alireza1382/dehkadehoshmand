package com.parisa00100.dehkadehhooshmand;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.parisa00100.dehkadehhooshmand.NewClasses.News;
import com.parisa00100.dehkadehhooshmand.NewClasses.Shop;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHoulder> {
    Context context;
    List<News> list;

    public NewsAdapter(Context context, List<News> list) {
        this.context=context;
        this.list = list;
    }


    @NonNull
    @Override
    public NewsAdapter.MyViewHoulder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.news, viewGroup, false);

        return new NewsAdapter.MyViewHoulder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull NewsAdapter.MyViewHoulder houlder, int position) {
        News news = list.get(position);
      Glide.with(context).load("http://idpz.ir/assets/images/news/" + news.getPic()).into(houlder.imageView);
//        Picasso.get().load("http://idpz.ir/assets/images/news/" + news.getPic()).into(houlder.imageView);

        houlder.title1.setText("پاکدشت");
        houlder.title2.setText(news.getTitle());
        houlder.tilte3.setText(news.getBody());
    }


    @Override
    public int getItemCount() {
        try {
            return list.size();
        } catch (Exception e) {
        }
        return 0;
    }


    public class MyViewHoulder extends RecyclerView.ViewHolder {
        RoundedImageView imageView;
        TextView title1;
        TextView title2;
        TextView tilte3;

        public MyViewHoulder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.img);
            title1 = itemView.findViewById(R.id.txt1);
            title2 = itemView.findViewById(R.id.txt2);
            tilte3 = itemView.findViewById(R.id.txt3);

        }
    }


}