package com.parisa00100.dehkadehhooshmand;

import android.content.Context;
import android.provider.DocumentsContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.parisa00100.dehkadehhooshmand.newmodel.Datum;

import java.util.List;

public class Adapterofeghamatclasses extends RecyclerView.Adapter<Adapterofeghamatclasses.ViewHoulderr> {
    Context context;
    List<com.parisa00100.dehkadehhooshmand.newmodel.Datum> list;


    public Adapterofeghamatclasses(Context context, List<com.parisa00100.dehkadehhooshmand.newmodel.Datum> list) {
        this.context = context;
        this.list = list;
    }


    @NonNull
    @Override
    public ViewHoulderr onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layoutofeghmatgah, viewGroup, false);
        return new ViewHoulderr(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoulderr viewHoulderr, int i) {
        Datum datum = list.get(i);
        Glide.with(context).load("http://idpz.ir/assets/images/villas/" + datum.getPic()).into(viewHoulderr.roundedImageView);
        viewHoulderr.ratingBar.setRating(datum.getScore());
        viewHoulderr.txtName.setText(datum.getName());
        viewHoulderr.txtPrice.setText(datum.getPrice()+" تومان");
        viewHoulderr.txtCity.setText(datum.getAdrs());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHoulderr extends RecyclerView.ViewHolder {
        RoundedImageView roundedImageView;
        RatingBar ratingBar;
        TextView txtName, txtPrice, txtCity;

        public ViewHoulderr(@NonNull View itemView) {
            super(itemView);
            roundedImageView = itemView.findViewById(R.id.roundimg);
            ratingBar = itemView.findViewById(R.id.ratingimg);
            txtName = itemView.findViewById(R.id.txtName);
            txtPrice = itemView.findViewById(R.id.txtPrice);
            txtCity = itemView.findViewById(R.id.txtCity);

        }


    }


}
