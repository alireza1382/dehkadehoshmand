package com.parisa00100.dehkadehhooshmand;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.parisa00100.dehkadehhooshmand.NewClasses.Villa;
import com.squareup.picasso.Picasso;

import java.util.List;


public class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.MyViewHoulder> {

    Context context;
    List<Villa> list;

    public RecycleAdapter(Context context , List<Villa> list) {
        this.context=context;
        this.list = list;
    }


    @NonNull
    @Override
    public MyViewHoulder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.recycleview_data, viewGroup, false);

        return new MyViewHoulder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHoulder houlder, int position) {
         Villa villas= list.get(position);

        Glide.with(context).load("http://idpz.ir/assets/images/villas/" + villas.getImage()).into(houlder.roundimg);

        houlder.txtCity.setText("تهران");
        houlder.txtPlace.setText(villas.getName());
        houlder.txtSize.setText(villas.getMeter()+" متر");
        houlder.txtNumber.setText(villas.getCapacity()+" نفر");
        houlder.txtPrice.setText(villas.getPrice()+" تومان");
        houlder.ratingimg.setRating((float) villas.getScore());
    }


    @Override
    public int getItemCount() {
        try {
            return list.size();
        } catch (Exception e) {
        }
        return 0;
    }


    public class MyViewHoulder extends RecyclerView.ViewHolder {

        RoundedImageView roundimg;
        TextView txtCity;
        TextView txtPlace;
        TextView txtSize;
        TextView txtNumber;
        TextView txtPrice;
        RatingBar ratingimg;

        public MyViewHoulder(@NonNull View itemView) {
            super(itemView);

            roundimg = itemView.findViewById(R.id.roundimg);
            txtCity = itemView.findViewById(R.id.txtCity);
            txtPlace = itemView.findViewById(R.id.txtPlace);
            txtSize = itemView.findViewById(R.id.txtSize);
            txtNumber = itemView.findViewById(R.id.txtNumber);
            txtPrice = itemView.findViewById(R.id.txtPrice);
            ratingimg = itemView.findViewById(R.id.ratingimg);


        }
    }


}
