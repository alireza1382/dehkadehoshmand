package com.parisa00100.dehkadehhooshmand;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.parisa00100.dehkadehhooshmand.NewClasses.Shop;

import java.util.List;

public class RecycleAdapter2 extends RecyclerView.Adapter<RecycleAdapter2.MyViewHoulder> {
    Context context;
    List<Shop> list;

    public RecycleAdapter2(Context context, List<Shop> list) {
        this.context = context;
        this.list = list;
    }


    @NonNull
    @Override
    public RecycleAdapter2.MyViewHoulder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.recycleview_data2, viewGroup, false);

        return new RecycleAdapter2.MyViewHoulder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RecycleAdapter2.MyViewHoulder houlder, int position) {
        Shop shop = list.get(position);
        Glide.with(context).load("http://idpz.ir/assets/images/shops/" + shop.getPic()).into(houlder.imageView);
        houlder.title1.setText(shop.getAddress());
        houlder.title2.setText(shop.getName());
        houlder.tilte3.setText(shop.getJkey());
    }


    @Override
    public int getItemCount() {
        try {
            return list.size();
        } catch (Exception e) {
        }
        return 0;
    }


    public class MyViewHoulder extends RecyclerView.ViewHolder {
        RoundedImageView imageView;
        TextView title1;
        TextView title2;
        TextView tilte3;

        public MyViewHoulder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.img);
            title1 = itemView.findViewById(R.id.txt1);
            title2 = itemView.findViewById(R.id.txt2);
            tilte3 = itemView.findViewById(R.id.txt3);

        }
    }


}
