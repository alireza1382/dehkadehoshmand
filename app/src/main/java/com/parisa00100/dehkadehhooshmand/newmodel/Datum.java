
package com.parisa00100.dehkadehhooshmand.newmodel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("owner")
    @Expose
    private String owner;
    @SerializedName("tel")
    @Expose
    private String tel;
    @SerializedName("meter")
    @Expose
    private Integer meter;
    @SerializedName("capacity")
    @Expose
    private Integer capacity;
    @SerializedName("options")
    @Expose
    private String options;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;
    @SerializedName("adrs")
    @Expose
    private String adrs;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("memo")
    @Expose
    private String memo;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("pic")
    @Expose
    private String pic;
    @SerializedName("score")
    @Expose
    private float score;

    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("room")
    @Expose
    private String room;
    @SerializedName("facility")
    @Expose
    private String facility;
    @SerializedName("perperson")
    @Expose
    private Integer perperson;
    @SerializedName("maxcapacity")
    @Expose
    private Integer maxcapacity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Integer getMeter() {
        return meter;
    }

    public void setMeter(Integer meter) {
        this.meter = meter;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getAdrs() {
        return adrs;
    }

    public void setAdrs(String adrs) {
        this.adrs = adrs;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }



    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public Integer getPerperson() {
        return perperson;
    }

    public void setPerperson(Integer perperson) {
        this.perperson = perperson;
    }

    public Integer getMaxcapacity() {
        return maxcapacity;
    }

    public void setMaxcapacity(Integer maxcapacity) {
        this.maxcapacity = maxcapacity;
    }

}
