package com.parisa00100.dehkadehhooshmand;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.parisa00100.dehkadehhooshmand.NewClasses.Villa;
import com.parisa00100.dehkadehhooshmand.newmodel.Datum;
import com.parisa00100.dehkadehhooshmand.newmodel.Example;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class Eghamatgah extends AppCompatActivity {

    List<com.parisa00100.dehkadehhooshmand.newmodel.Datum> list = new ArrayList<com.parisa00100.dehkadehhooshmand.newmodel.Datum>();
    LinearLayoutManager linearLayoutManager;
    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayout linearLayout;
    ImageButton btnBack;
    List<Villa> villas = new ArrayList<Villa>();
    RecyclerView recyclerView;
    Adapterofeghamatclasses adapterofeghamatclasses;
    ProgressBar progressBar;
    String nexturl;
    String first;
    String last;
    int currentPage;
    int lastPage;
    int prePage;
    boolean flag = true;
    AlertDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eghamatgah);
        progressBar = findViewById(R.id.progressbar);
        btnBack = findViewById(R.id.btnBack);
        recyclerView = findViewById(R.id.recycle);
        linearLayout = findViewById(R.id.view2);
        swipeRefreshLayout = findViewById(R.id.jobRefresh);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Eghamatgah.this, MainActivity.class);
                startActivity(intent);
            }
        });

        dialog = new AlertDialog();


//        int firstCompletelyVisibleItemPosition = 0;
//        firstCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        linearLayoutManager = new LinearLayoutManager(Eghamatgah.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        eghamat("http://admin.idpz.ir/api/V2/take_villas");

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);


                if (currentPage != lastPage) {

                    if (!recyclerView.canScrollVertically(1)) {
                        if (dialog.isNetworkAvaliable(Eghamatgah.this)) {
                            linearLayout.setVisibility(View.VISIBLE);
                            eghamat(nexturl);



                        }

                    }
                }

            }

        });





     /*   recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (lastPage != currentPage) {
                    // progress.setVisibility(View.GONE);

                    int lastCompletelyVisibleItemPosition = 0;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();

                    if (lastCompletelyVisibleItemPosition + 1 == villas.size()) {
                        String page = "";
                        //   page = nextUrl.replace("https://api.hambazisho.ir/api/usersVideos?page=", "");
                        //  getActorVideoListFromServer(page);


                        if (dialog.isNetworkAvaliable(Eghamatgah.this)) {
                            if (flag) {
                                progressBar.setVisibility(View.VISIBLE);

                                eghamat(nexturl);
                                flag = false;
                            }

                        } else return;

                    }
                }

            }

        });
*/


    }

    public void eghamat(String url) {

        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        params.put("tag", "");// geo ostan
        params.put("lat", "");
        params.put("lng", "");
        params.put("state", "");
        params.put("ostan", "");
        params.put("api_token", "wVR3RtYivwvG1AzfNOaYRZteDFiqhgfGAeDuNAV4EuWHauUzqQjbZnYX6Uvri75r");

        asyncHttpClient.post(url, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                swipeRefreshLayout.setRefreshing(false);

                Toast.makeText(Eghamatgah.this, "Error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                swipeRefreshLayout.setRefreshing(false);

                recyclerView.animate().translationX(0f).setDuration(1000);
                linearLayout.setVisibility(View.GONE);
                Gson gson = new Gson();

                com.parisa00100.dehkadehhooshmand.newmodel.Example data = gson.fromJson(responseString, com.parisa00100.dehkadehhooshmand.newmodel.Example.class);

                prePage = data.getMeta().getPerPage();
                nexturl = data.getLinks().getNext();
                currentPage = data.getMeta().getCurrentPage();
                lastPage = data.getMeta().getLastPage();
                list = data.getData();

                adapterofeghamatclasses = new Adapterofeghamatclasses(Eghamatgah.this, list);
                recyclerView.setAdapter(adapterofeghamatclasses);

            }


        });


    }


}




