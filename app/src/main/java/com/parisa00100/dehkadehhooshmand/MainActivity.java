package com.parisa00100.dehkadehhooshmand;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.parisa00100.dehkadehhooshmand.NewClasses.Main;
import com.parisa00100.dehkadehhooshmand.NewClasses.Main1;
import com.parisa00100.dehkadehhooshmand.NewClasses.News;
import com.parisa00100.dehkadehhooshmand.NewClasses.Shop;
import com.parisa00100.dehkadehhooshmand.NewClasses.Social;
import com.parisa00100.dehkadehhooshmand.NewClasses.Villa;
import com.parisa00100.dehkadehhooshmand.NewClasses.Visitplace;

import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


import cz.msebera.android.httpclient.Header;
import ru.tinkoff.scrollingpagerindicator.ScrollingPagerIndicator;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    ProgressBar progressBar;
    ScrollingPagerIndicator scrollingPagerIndicator;
    RecyclerView recyclerView, recyclerView1, recyclerView2, recyclerView4, recyclerView5;
    RecycleAdapter recycleAdapter;
    EditText editText;
    LinearLayout linearLayout;
    RecycleAdapter2 recycleAdapter2;
    NewsAdapter newsAdapter;
    ScrollView scrollView;
    RecycleAdapter3 recycleAdapterr;
    SocialAdapter socialAdapter;
    Dialog dialog;
    CardView cardView;
    SliderLayout sliderLayout;
    HashMap<String, String> Hash_file_maps;
    RelativeLayout relativeLayout, relativeLayout1, relat, bigRelativeLayout;
    Button btnVilla;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //btns

        btnVilla = findViewById(R.id.btnVilla);


        //btns


        //onclickes of btns
        btnVilla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Eghamatgah.class);
                startActivity(intent);
            }
        });


        //onclickes of btns


        cardView = findViewById(R.id.card);
        scrollView = findViewById(R.id.scroll);
        editText = findViewById(R.id.search_edt);
        linearLayout = findViewById(R.id.li1);
        progressBar = findViewById(R.id.progressbar);
        linearLayout.setFocusableInTouchMode(true);
        linearLayout.setFocusable(true);
        linearLayout.requestFocus();
        relativeLayout1 = findViewById(R.id.rel1);
        relat = findViewById(R.id.view2);
        bigRelativeLayout = findViewById(R.id.bigRelativeLayout);
        //        BannerSlider bannerSlider = (BannerSlider) findViewById(R.id.banner_slider1);

        //add banner using image url
        //  banners.add(new RemoteBanner("Put banner image url here ..."));

//        List<Banner> banners = new ArrayList<>();

//        banners.add(new DrawableBanner(R.drawable.img9));
//        banners.add(new DrawableBanner(R.drawable.img10));
//        banners.add(new DrawableBanner(R.drawable.img6));
//        bannerSlider.setBanners(banners);

        AlertDialog alertDialog = new AlertDialog();

        if (isNetworkAvaliable(this)) {

            banner();
            items();
        } else {
            alertDialog.showDialog(this);

        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }


    @Override
    protected void onResume() {
        AlertDialog alertDialog = new AlertDialog();
        super.onResume();


        if (isNetworkAvaliable(this)) {

            banner();
            items();
        } else {
            alertDialog.showDialog(this);

        }


    }

//    @Override
//    protected void onStop() {
//
//        sliderLayout.stopAutoCycle();
//
//        super.onStop();
//
//
//    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    public void banner() {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        String url = "http://admin.idpz.ir/api/app_banner";
        params.put("api_token", "wVR3RtYivwvG1AzfNOaYRZteDFiqhgfGAeDuNAV4EuWHauUzqQjbZnYX6Uvri75r");
        params.put("state", "");


        client.post(url, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                try {


                    Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                Gson gson = new Gson();
                Data data = gson.fromJson(responseString, Data.class);

                for (final Video video : data.getVideos()) {

                    Hash_file_maps = new HashMap<String, String>();
                    sliderLayout = (SliderLayout) findViewById(R.id.slider);


                    Hash_file_maps.put("  " + video.getTitle(), "http://idpz.ir/assets/images/videos/" + video.getPic());


                    for (String name : Hash_file_maps.keySet()) {

                        TextSliderView textSliderView = new TextSliderView(MainActivity.this);
                        textSliderView
                                .description(name)
                                .image(Hash_file_maps.get(name))
                                .setScaleType(BaseSliderView.ScaleType.Fit)
                                .setOnSliderClickListener(MainActivity.this);
                        textSliderView.bundle(new Bundle());
                        textSliderView.getBundle()
                                .putString("extra", name);

                        sliderLayout.addSlider(textSliderView);

                    }
                    sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                    sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                    sliderLayout.setCustomAnimation(new DescriptionAnimation());
                    sliderLayout.setDuration(3000);
                    sliderLayout.addOnPageChangeListener(MainActivity.this);


                }

            }
        });


    }


    public void items() {
        AsyncHttpClient httpClient = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        String url = "http://admin.idpz.ir/api/app_main";
        params.put("api_token", "wVR3RtYivwvG1AzfNOaYRZteDFiqhgfGAeDuNAV4EuWHauUzqQjbZnYX6Uvri75r");
        params.put("state", "050026");
        httpClient.post(url, params, new TextHttpResponseHandler() {


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progressBar.setVisibility(View.GONE);
                relat.animate().alpha(0);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                progressBar.setVisibility(View.GONE);
                relat.animate().alpha(0).setDuration(statusCode);
                Gson gson = new Gson();
                Main main = gson.fromJson(responseString, Main.class);
                for (Villa villa : main.getMain().getVillas()) {
                    List<Villa> list = new ArrayList<Villa>();
                    list = main.getMain().getVillas();
                    recyclerView = findViewById(R.id.recycle1);
                    recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, true));
                    recycleAdapter = new RecycleAdapter(MainActivity.this, list);
                    recyclerView.setAdapter(recycleAdapter);
                }
                for (Shop shop : main.getMain().getShops()) {
                    List<Shop> list1 = new ArrayList<Shop>();
                    list1 = main.getMain().getShops();
                    recyclerView1 = findViewById(R.id.recycle2);
                    recyclerView1.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, true));
                    recycleAdapter2 = new RecycleAdapter2(MainActivity.this, list1);
                    recyclerView1.setAdapter(recycleAdapter2);
                }
                for (Visitplace visitplace : main.getMain().getVisitplace()) {
                    List<Visitplace> list2 = new ArrayList<Visitplace>();
                    list2 = main.getMain().getVisitplace();
                    recyclerView2 = findViewById(R.id.recycle3);
                    recyclerView2.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, true));
                    recycleAdapterr = new RecycleAdapter3(MainActivity.this, list2);
                    recyclerView2.setAdapter(recycleAdapterr);
                }
                for (Social social : main.getMain().getSocials()) {
                    List<Social> list3 = new ArrayList<Social>();
                    list3 = main.getMain().getSocials();
                    recyclerView4 = findViewById(R.id.recycle4);
                    recyclerView4.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, true));
                    socialAdapter = new SocialAdapter(MainActivity.this, list3);
                    recyclerView4.setAdapter(socialAdapter);
                }
                for (News news : main.getMain().getNews()) {
                    List<News> list4 = new ArrayList<News>();
                    list4 = main.getMain().getNews();
                    recyclerView5 = findViewById(R.id.recycle5);
                    recyclerView5.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, true));
                    newsAdapter = new NewsAdapter(MainActivity.this, list4);
                    recyclerView5.setAdapter(newsAdapter);
                }
            }
        });
    }


    public boolean isNetworkAvaliable(Context ctx) {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if ((connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED)
                || (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .getState() == NetworkInfo.State.CONNECTED)) {
            return true;
        } else {
            return false;
        }
    }

//    public void alertDialog() {
//        dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(false);
//        dialog.setContentView(R.layout.alert_dialog);
//
//
//        Button dialogWarning = dialog.findViewById(R.id.try_again);
//        Button dialogExit = dialog.findViewById(R.id.Exit);
//
//        dialogWarning.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                if (isNetworkAvaliable(MainActivity.this) == false) {
//
//                    Toast.makeText(MainActivity.this, "خطا در اتصال به اینترنت", Toast.LENGTH_SHORT).show();
//
//
//                } else {
//                    dialog.dismiss();
//
//                    banner();
//                    items();
//
//                }
//
//
//            }
//        });
//
//        dialogExit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//
//            }
//        });
//
//
//        dialog.show();
//
//
//    }


}










