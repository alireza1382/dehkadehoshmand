package com.parisa00100.dehkadehhooshmand;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.parisa00100.dehkadehhooshmand.NewClasses.Visitplace;

import java.util.List;

public class RecycleAdapter3 extends RecyclerView.Adapter<RecycleAdapter3.MyViewHoulder> {

    Context context;
    List<Visitplace> list;

    public RecycleAdapter3(Context context, List<Visitplace> list) {
        this.context = context;
        this.list = list;
    }


    @NonNull
    @Override
    public RecycleAdapter3.MyViewHoulder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.recycleview_data3, viewGroup, false);

        return new RecycleAdapter3.MyViewHoulder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RecycleAdapter3.MyViewHoulder houlder, int position) {
        Visitplace visitplace = list.get(position);
        Glide.with(context).load("http://idpz.ir/assets/images/visitplaces/" + visitplace.getImage()).into(houlder.imageView);

        houlder.title1.setText(visitplace.getAddress());
        houlder.title2.setText(visitplace.getName());
        houlder.tilte3.setText(visitplace.getPyear()+"...");
    }


    @Override
    public int getItemCount() {
        try {
            return list.size();
        } catch (Exception e) {
        }
        return 0;
    }


    public class MyViewHoulder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView title1;
        TextView title2;
        TextView tilte3;

        public MyViewHoulder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.img);
            title1 = itemView.findViewById(R.id.txt1);
            title2 = itemView.findViewById(R.id.txt2);
            tilte3 = itemView.findViewById(R.id.txt3);

        }
    }


}
