package com.parisa00100.dehkadehhooshmand;

public class AmountsOfSlierLayout {


    int img;
    int video;

    public AmountsOfSlierLayout(int img, int video) {
        this.img = img;
        this.video = video;
    }


    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public int getVideo() {
        return video;
    }

    public void setVideo(int video) {
        this.video = video;
    }
}
