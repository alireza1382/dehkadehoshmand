
package com.parisa00100.dehkadehhooshmand.NewClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Good {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("sold")
    @Expose
    private Object sold;
    @SerializedName("cat")
    @Expose
    private Object cat;
    @SerializedName("rate")
    @Expose
    private Object rate;
    @SerializedName("special")
    @Expose
    private Object special;
    @SerializedName("num_score")
    @Expose
    private Object numScore;
    @SerializedName("soghati")
    @Expose
    private Long soghati;
    @SerializedName("web")
    @Expose
    private Object web;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("comment")
    @Expose
    private Long comment;
    @SerializedName("buy_price")
    @Expose
    private String buyPrice;
    @SerializedName("old_price")
    @Expose
    private String oldPrice;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("score")
    @Expose
    private Long score;
    @SerializedName("tel")
    @Expose
    private String tel;
    @SerializedName("is_exist")
    @Expose
    private Long isExist;
    @SerializedName("garanti")
    @Expose
    private Object garanti;
    @SerializedName("memo")
    @Expose
    private String memo;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("actime")
    @Expose
    private String actime;
    @SerializedName("user")
    @Expose
    private Object user;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("owner")
    @Expose
    private Long owner;
    @SerializedName("pub")
    @Expose
    private Long pub;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Object getSold() {
        return sold;
    }

    public void setSold(Object sold) {
        this.sold = sold;
    }

    public Object getCat() {
        return cat;
    }

    public void setCat(Object cat) {
        this.cat = cat;
    }

    public Object getRate() {
        return rate;
    }

    public void setRate(Object rate) {
        this.rate = rate;
    }

    public Object getSpecial() {
        return special;
    }

    public void setSpecial(Object special) {
        this.special = special;
    }

    public Object getNumScore() {
        return numScore;
    }

    public void setNumScore(Object numScore) {
        this.numScore = numScore;
    }

    public Long getSoghati() {
        return soghati;
    }

    public void setSoghati(Long soghati) {
        this.soghati = soghati;
    }

    public Object getWeb() {
        return web;
    }

    public void setWeb(Object web) {
        this.web = web;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getComment() {
        return comment;
    }

    public void setComment(Long comment) {
        this.comment = comment;
    }

    public String getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(String buyPrice) {
        this.buyPrice = buyPrice;
    }

    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Long getIsExist() {
        return isExist;
    }

    public void setIsExist(Long isExist) {
        this.isExist = isExist;
    }

    public Object getGaranti() {
        return garanti;
    }

    public void setGaranti(Object garanti) {
        this.garanti = garanti;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public String getActime() {
        return actime;
    }

    public void setActime(String actime) {
        this.actime = actime;
    }

    public Object getUser() {
        return user;
    }

    public void setUser(Object user) {
        this.user = user;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Long getOwner() {
        return owner;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }

    public Long getPub() {
        return pub;
    }

    public void setPub(Long pub) {
        this.pub = pub;
    }

}
