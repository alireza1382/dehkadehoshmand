package com.parisa00100.dehkadehhooshmand.NewClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Main {

    @SerializedName("main")
    @Expose
    private Main1 main;
    public Main1 getMain() {
        return main;
    }
    public void setMain(Main1 main) {
        this.main = main;
    }
}