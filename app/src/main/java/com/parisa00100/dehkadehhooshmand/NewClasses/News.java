
package com.parisa00100.dehkadehhooshmand.NewClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class News {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("sender")
    @Expose
    private String sender;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("pic")
    @Expose
    private String pic;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("ndate")
    @Expose
    private String ndate;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("view")
    @Expose
    private String view;
    @SerializedName("pub")
    @Expose
    private String pub;
    @SerializedName("rmv")
    @Expose
    private Long rmv;
    @SerializedName("userid")
    @Expose
    private Long userid;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("actime")
    @Expose
    private Long actime;
    @SerializedName("gov")
    @Expose
    private String gov;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getNdate() {
        return ndate;
    }

    public void setNdate(String ndate) {
        this.ndate = ndate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getPub() {
        return pub;
    }

    public void setPub(String pub) {
        this.pub = pub;
    }

    public Long getRmv() {
        return rmv;
    }

    public void setRmv(Long rmv) {
        this.rmv = rmv;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Long getActime() {
        return actime;
    }

    public void setActime(Long actime) {
        this.actime = actime;
    }

    public String getGov() {
        return gov;
    }

    public void setGov(String gov) {
        this.gov = gov;
    }

}
