
package com.parisa00100.dehkadehhooshmand.NewClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.util.List;

public class Villa {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("owner")
    @Expose
    private String owner;
    @SerializedName("tel")
    @Expose
    private String tel;
    @SerializedName("meter")
    @Expose
    private Long meter;
    @SerializedName("capacity")
    @Expose
    private Long capacity;
    @SerializedName("options")
    @Expose
    private String options;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;
    @SerializedName("adrs")
    @Expose
    private String adrs;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("memo")
    @Expose
    private String memo;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("score")
    @Expose
    private double score;

    @SerializedName("area")
    @Expose
    private Long area;
    @SerializedName("room")
    @Expose
    private Long room;
    @SerializedName("facility")
    @Expose
    private Long facility;
    @SerializedName("alat")
    @Expose
    private Long alat;
    @SerializedName("alng")
    @Expose
    private Long alng;
    @SerializedName("aename")
    @Expose
    private Long aename;
    @SerializedName("afname")
    @Expose
    private Long afname;
    @SerializedName("server")
    @Expose
    private Long server;
    @SerializedName("perperson")
    @Expose
    private Long perperson;
    @SerializedName("maxcapacity")
    @Expose
    private Long maxcapacity;
//    @SerializedName("comment")
//    @Expose
//    private List<Comment> comment ;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Long getMeter() {
        return meter;
    }

    public void setMeter(Long meter) {
        this.meter = meter;
    }

    public Long getCapacity() {
        return capacity;
    }

    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getAdrs() {
        return adrs;
    }

    public void setAdrs(String adrs) {
        this.adrs = adrs;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
//
//    public List<Comment> getComment() {
//        return comment;
//    }
//
//    public void setComment(List<Comment> comment) {
//        this.comment = comment;
//    }


    public Long getArea() {
        return area;
    }

    public void setArea(Long area) {
        this.area = area;
    }

    public Long getRoom() {
        return room;
    }

    public void setRoom(Long room) {
        this.room = room;
    }

    public Long getFacility() {
        return facility;
    }

    public void setFacility(Long facility) {
        this.facility = facility;
    }

    public Long getAlat() {
        return alat;
    }

    public void setAlat(Long alat) {
        this.alat = alat;
    }

    public Long getAlng() {
        return alng;
    }

    public void setAlng(Long alng) {
        this.alng = alng;
    }

    public Long getAename() {
        return aename;
    }

    public void setAename(Long aename) {
        this.aename = aename;
    }

    public Long getAfname() {
        return afname;
    }

    public void setAfname(Long afname) {
        this.afname = afname;
    }

    public Long getServer() {
        return server;
    }

    public void setServer(Long server) {
        this.server = server;
    }

    public Long getPerperson() {
        return perperson;
    }

    public void setPerperson(Long perperson) {
        this.perperson = perperson;
    }

    public Long getMaxcapacity() {
        return maxcapacity;
    }

    public void setMaxcapacity(Long maxcapacity) {
        this.maxcapacity = maxcapacity;
    }

}
