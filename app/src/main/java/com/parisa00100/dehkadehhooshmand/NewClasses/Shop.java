
package com.parisa00100.dehkadehhooshmand.NewClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shop {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("owner")
    @Expose
    private String owner;
    @SerializedName("ownername")
    @Expose
    private String ownername;
    @SerializedName("tel")
    @Expose
    private String tel;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("pic")
    @Expose
    private String pic;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("jlat")
    @Expose
    private String jlat;
    @SerializedName("jlng")
    @Expose
    private String jlng;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("jkey")
    @Expose
    private String jkey;
    @SerializedName("memo")
    @Expose
    private Object memo;
    @SerializedName("pub")
    @Expose
    private String pub;
    @SerializedName("userid")
    @Expose
    private Object userid;
    @SerializedName("action")
    @Expose
    private Object action;
    @SerializedName("actime")
    @Expose
    private Object actime;
    @SerializedName("rmv")
    @Expose
    private String rmv;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("expire")
    @Expose
    private String expire;
    @SerializedName("distance")
    @Expose
    private Long distance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwnername() {
        return ownername;
    }

    public void setOwnername(String ownername) {
        this.ownername = ownername;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getJlat() {
        return jlat;
    }

    public void setJlat(String jlat) {
        this.jlat = jlat;
    }

    public String getJlng() {
        return jlng;
    }

    public void setJlng(String jlng) {
        this.jlng = jlng;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getJkey() {
        return jkey;
    }

    public void setJkey(String jkey) {
        this.jkey = jkey;
    }

    public Object getMemo() {
        return memo;
    }

    public void setMemo(Object memo) {
        this.memo = memo;
    }

    public String getPub() {
        return pub;
    }

    public void setPub(String pub) {
        this.pub = pub;
    }

    public Object getUserid() {
        return userid;
    }

    public void setUserid(Object userid) {
        this.userid = userid;
    }

    public Object getAction() {
        return action;
    }

    public void setAction(Object action) {
        this.action = action;
    }

    public Object getActime() {
        return actime;
    }

    public void setActime(Object actime) {
        this.actime = actime;
    }

    public String getRmv() {
        return rmv;
    }

    public void setRmv(String rmv) {
        this.rmv = rmv;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public Long getDistance() {
        return distance;
    }

    public void setDistance(Long distance) {
        this.distance = distance;
    }

}
