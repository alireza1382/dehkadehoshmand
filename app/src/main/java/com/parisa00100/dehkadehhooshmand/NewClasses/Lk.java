
package com.parisa00100.dehkadehhooshmand.NewClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lk {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("soid")
    @Expose
    private Long soid;
    @SerializedName("usr")
    @Expose
    private String usr;
    @SerializedName("lk")
    @Expose
    private Long lk;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSoid() {
        return soid;
    }

    public void setSoid(Long soid) {
        this.soid = soid;
    }

    public String getUsr() {
        return usr;
    }

    public void setUsr(String usr) {
        this.usr = usr;
    }

    public Long getLk() {
        return lk;
    }

    public void setLk(Long lk) {
        this.lk = lk;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

}
