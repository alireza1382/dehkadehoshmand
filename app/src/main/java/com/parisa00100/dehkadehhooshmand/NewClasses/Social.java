
package com.parisa00100.dehkadehhooshmand.NewClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Social {

    @SerializedName("soid")
    @Expose
    private Long soid;
    @SerializedName("soname")
    @Expose
    private Object soname;
    @SerializedName("sophone")
    @Expose
    private String sophone;
    @SerializedName("sotext")
    @Expose
    private String sotext;
    @SerializedName("soserial")
    @Expose
    private Object soserial;
    @SerializedName("sostatus")
    @Expose
    private Long sostatus;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("soincharge")
    @Expose
    private String soincharge;
    @SerializedName("sodone")
    @Expose
    private Long sodone;
    @SerializedName("sopub")
    @Expose
    private Long sopub;
    @SerializedName("sotime")
    @Expose
    private String sotime;
    @SerializedName("pic")
    @Expose
    private String pic;
    @SerializedName("comment")
    @Expose
    private Long comment;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;
    @SerializedName("gov")
    @Expose
    private Long gov;
    @SerializedName("view")
    @Expose
    private Long view;
    @SerializedName("seen")
    @Expose
    private Long seen;
    @SerializedName("usrimg")
    @Expose
    private Object usrimg;
    @SerializedName("lk")
    @Expose
    private Lk lk;

    public Long getSoid() {
        return soid;
    }

    public void setSoid(Long soid) {
        this.soid = soid;
    }

    public Object getSoname() {
        return soname;
    }

    public void setSoname(Object soname) {
        this.soname = soname;
    }

    public String getSophone() {
        return sophone;
    }

    public void setSophone(String sophone) {
        this.sophone = sophone;
    }

    public String getSotext() {
        return sotext;
    }

    public void setSotext(String sotext) {
        this.sotext = sotext;
    }

    public Object getSoserial() {
        return soserial;
    }

    public void setSoserial(Object soserial) {
        this.soserial = soserial;
    }

    public Long getSostatus() {
        return sostatus;
    }

    public void setSostatus(Long sostatus) {
        this.sostatus = sostatus;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSoincharge() {
        return soincharge;
    }

    public void setSoincharge(String soincharge) {
        this.soincharge = soincharge;
    }

    public Long getSodone() {
        return sodone;
    }

    public void setSodone(Long sodone) {
        this.sodone = sodone;
    }

    public Long getSopub() {
        return sopub;
    }

    public void setSopub(Long sopub) {
        this.sopub = sopub;
    }

    public String getSotime() {
        return sotime;
    }

    public void setSotime(String sotime) {
        this.sotime = sotime;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Long getComment() {
        return comment;
    }

    public void setComment(Long comment) {
        this.comment = comment;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Long getGov() {
        return gov;
    }

    public void setGov(Long gov) {
        this.gov = gov;
    }

    public Long getView() {
        return view;
    }

    public void setView(Long view) {
        this.view = view;
    }

    public Long getSeen() {
        return seen;
    }

    public void setSeen(Long seen) {
        this.seen = seen;
    }

    public Object getUsrimg() {
        return usrimg;
    }

    public void setUsrimg(Object usrimg) {
        this.usrimg = usrimg;
    }

    public Lk getLk() {
        return lk;
    }

    public void setLk(Lk lk) {
        this.lk = lk;
    }

}
