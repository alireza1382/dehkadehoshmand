
package com.parisa00100.dehkadehhooshmand.NewClasses;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Main1 {

    @SerializedName("socials")
    @Expose
    private List<Social> socials ;
    @SerializedName("villas")
    @Expose
    private List<Villa> villas ;
    @SerializedName("visitplace")
    @Expose
    private List<Visitplace> visitplace ;
    @SerializedName("news")
    @Expose
    private List<News> news ;
    @SerializedName("shops")
    @Expose
    private List<Shop> shops ;
    @SerializedName("goods")
    @Expose
    private List<Good> goods ;

    public List<Social> getSocials() {
        return socials;
    }

    public void setSocials(List<Social> socials) {
        this.socials = socials;
    }

    public List<Villa> getVillas() {
        return villas;
    }

    public void setVillas(List<Villa> villas) {
        this.villas = villas;
    }

    public List<Visitplace> getVisitplace() {
        return visitplace;
    }

    public void setVisitplace(List<Visitplace> visitplace) {
        this.visitplace = visitplace;
    }

    public List<News> getNews() {
        return news;
    }

    public void setNews(List<News> news) {
        this.news = news;
    }

    public List<Shop> getShops() {
        return shops;
    }

    public void setShops(List<Shop> shops) {
        this.shops = shops;
    }

    public List<Good> getGoods() {
        return goods;
    }

    public void setGoods(List<Good> goods) {
        this.goods = goods;
    }

}
