
package com.parisa00100.dehkadehhooshmand.NewClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Visitplace {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("pyear")
    @Expose
    private String pyear;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("tel")
    @Expose
    private Long tel;
    @SerializedName("ticket")
    @Expose
    private Long ticket;
    @SerializedName("pdays")
    @Expose
    private Long pdays;
    @SerializedName("phours")
    @Expose
    private Long phours;
    @SerializedName("memo")
    @Expose
    private Long memo;
    @SerializedName("alat")
    @Expose
    private Long alat;
    @SerializedName("alng")
    @Expose
    private Long alng;
    @SerializedName("aename")
    @Expose
    private Long aename;
    @SerializedName("afname")
    @Expose
    private Long afname;
    @SerializedName("distance")
    @Expose
    private Object distance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPyear() {
        return pyear;
    }

    public void setPyear(String pyear) {
        this.pyear = pyear;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getTel() {
        return tel;
    }

    public void setTel(Long tel) {
        this.tel = tel;
    }

    public Long getTicket() {
        return ticket;
    }

    public void setTicket(Long ticket) {
        this.ticket = ticket;
    }

    public Long getPdays() {
        return pdays;
    }

    public void setPdays(Long pdays) {
        this.pdays = pdays;
    }

    public Long getPhours() {
        return phours;
    }

    public void setPhours(Long phours) {
        this.phours = phours;
    }

    public Long getMemo() {
        return memo;
    }

    public void setMemo(Long memo) {
        this.memo = memo;
    }

    public Long getAlat() {
        return alat;
    }

    public void setAlat(Long alat) {
        this.alat = alat;
    }

    public Long getAlng() {
        return alng;
    }

    public void setAlng(Long alng) {
        this.alng = alng;
    }

    public Long getAename() {
        return aename;
    }

    public void setAename(Long aename) {
        this.aename = aename;
    }

    public Long getAfname() {
        return afname;
    }

    public void setAfname(Long afname) {
        this.afname = afname;
    }

    public Object getDistance() {
        return distance;
    }

    public void setDistance(Object distance) {
        this.distance = distance;
    }

}
